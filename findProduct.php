<?php
include 'simple_html_dom.php';    // библиотека для парсинга

// ********* константы режимов парсинга
define('TABLENAME', 'href');       // имя таблицы где храним ссылки
define('LIMIT', '10');              // количество ссылок получаемое из бд за одну загрузку скрипта

// ********* константы для подключения к бд
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', 'flexi140');
define('DB_NAME', 'dbkipr');

// получаем статусы из таблицы
$arrInTable = array();
$isInTable = Request("SELECT name FROM state;");
if ($isInTable) {

    for ($i = 0; $i < $isInTable->num_rows; $i++) {
        $row = $isInTable->fetch_assoc();
        $key = $row['name'];
        $val = implode(explode(" ", $key));
        $arrInTable[$key] = $val;
    }
}
// характеристики, со значениями в таблицах (id)
$arrNameOptions = array(
    'Тип недвижимости' => 'type',
    'Спальни' => 'bedrooms',
    'Мебель' => 'furniture',
    'Бассейн' => 'swimming'
);

// характеристики с готовыми данными(bool)
$arrIsExtraOptions = array(
    'Кондиционер' => 'isAir_conditioning',
    'Веранда' => 'isVeranda',
    'Детский стул' => 'isChild_chair',
    'Детская кроватка' => 'isCot',
    'Система безопасности' => 'Safety_system',
    'Балкон' => 'isBalcony',
    'Пол с подогревом' => 'Heating_floor',
    'Детская площадка' => 'isPlayground',
    'Центральное отопление' => 'isCentral_heating',
    'Солнечная батарея' => 'isSolar_battery',
    'Посудомоечная машина' => 'isDishwasher',
    'Парковка' => 'isParking',
    'Обустроенная кухня' => 'Equipped_kitchen',
    'Разрешено проживание с животными' => 'isPet',
    'Кладовая' => 'isPantry',
    'Стиральная машина' => 'isWasher',
    'TV' => 'isTV',
    'Интернет' => 'isInternet',
    'Камин' => 'isFireplace',
    'Площадка для барбекю' => 'IsBBQ',
    'Сад' => 'isGarden',
    'Беседка' => 'isArbor',
    'Сауна' => 'isSauna',
    'Джакузи' => 'isJacuzzi',
    'Теннисный корт' => 'isTennis_court'
);

// характеристики с готовыми данными(int)
$arrIsNumData = array(
    'Расстояние до моря' => 'seaDistance',
    'Крытая площадь' => 'coveredSquare',
    'Площадь участка' => 'allSquare',
    'Расстояние до супермаркета' => 'shopDistance',
);

// функция получает имя для поиска и имя таблицы, возврашает id
function GetId($nameRequest, $tableName)
{
    $id = null;
    $isInTable = Request("SELECT id FROM " . $tableName . " WHERE name ='" . trim($nameRequest) . "';");
    if ($isInTable) {
        $row = $isInTable->fetch_assoc();
        $id = $row['id'];
    }
    return $id;
}

//__________________ получаем id статуса
function GetStatus($dom, $arrInTable)
{
    $str = null;
    $status = $dom->find('span.text-info-status-prod');
    if (count($status) == 0) {
        echo "The selector GetStatus status failed" . "<br>";
        return null;
    } else {
        $strSt = "";
        foreach ($status as $val)
            $strSt = implode(explode(" ", $val->parent()));
        $strSt = explode(":", substr($strSt, 100));
        foreach ($arrInTable as $key => $val) {
            if (stripos($strSt[1], $val)) {
                $str = $key;
                break;
            }
        }
        $str = GetId($str, 'state');
    }
    return $str;
}

//__________________ получаем название
function GetName($dom)
{
    $str = null;
    $arrSearchResults = $dom->find('h1');
    if (count($arrSearchResults) == 0) {
        echo "The selector GetName status failed" . "<br>";
        return null;
    } else
        $str = $arrSearchResults[0]->plaintext;
    return $str;
}

//__________________ получаем id региона
function GetIdRegion($dom)
{
    $str = null;
    $arrSearchResults = $dom->find('li.loc span.text-info');
    if (count($arrSearchResults) == 0) {
        echo "The selector GetIdRegion status failed" . "<br>";
        return null;
    } else {
        foreach ($arrSearchResults as $element) {
            $arrCountryRegion = explode(",", $element->plaintext);
            if (empty($arrCountryRegion[1]) || $arrCountryRegion[1] == "") {
                $str = GetId($arrCountryRegion[0], 'region');
                //continue;
            } else {
                $str = GetId($arrCountryRegion[1], 'region');
            }
        }
    }
    return $str;
}

//__________________ получаем цену
function GetPrice($dom)
{
    $str = null;
    $arrSearchResults = $dom->find('li. span.main-price');
    if (count($arrSearchResults) == 0) {
        echo "The selector GetPrice status failed" . "<br>";
        return null;
    } else {
        $str = trim(mb_substr($arrSearchResults[0]->plaintext, 2));    // удаляем символы с начала строки
    }
    return $str;
}

//__________________ получаем характеристики
function GetTypeId($dom)
{
    $arrSearchResults = $dom->find('div.table-responsive table tbody'); // находим всю таблицу со всеми характеристиками
    if (count($arrSearchResults) == 0) {
        echo "The selector GetTypeId status failed" . "<br>";
        return null;
    } else {
        $arrTypes = array();
        foreach ($arrSearchResults[0]->find('td') as $val)             // находим строки таблицы
            $arrTypes[] = $val->plaintext;

        $arrTypes = array_filter($arrTypes, function ($el) {           // убираем пустые элементы
            return !empty($el);
        });

        $arr = array();                                               // основные параметры
        $arrTypes = array_chunk($arrTypes, 2);                   // разбиваем на несколько массивов по 2
        foreach ($arrTypes as $key => $val) {
            $arrStr = explode(":", implode($val));
            $arr[$arrStr[0]] = $arrStr[1];
        }

        $arrlastType = explode("&bull", array_pop($arr));            // дополнительные параметры
        foreach ($arrlastType as $key => $val) {
            $arrlastType[$key] = trim(str_replace(";", "", $val));
        }

        // ___ получаем id из таблиц по имени
        global $arrNameOptions;                                       // __ массив с опциями которые есть в таблицах
        $arrTypeId = array();
        foreach ($arrNameOptions as $key => $val) {                     //  $val название таблицы
            if (array_key_exists($key, $arr)) {
                $name = $arr[$key];                                   //  имя для поиска id
                $arrTypeId['id_' . $val] = GetId($name, $val);
            } else
                $arrTypeId['id_' . $val] = GetId('no', $val);
        }

        // ___ получаем доп опции (yes/no) из таблиц по имени
        global $arrIsExtraOptions;                                    // __ массив с готовыми данными (bool)
        $arrTypeExtraBool = array();
        foreach ($arrIsExtraOptions as $key => $val) {
            if (in_array($key, $arrlastType))
                $arrTypeExtraBool['is_' . $val] = 'yes';
            else
                $arrTypeExtraBool['is_' . $val] = 'no';
        }

        // ___ получаем числовые данные
        global $arrIsNumData;                                         // __ массив с готовыми данными (числами)
        $arrTypeExtraInt = array();
        foreach ($arrIsNumData as $key => $val) {
            if (array_key_exists($key, $arr)) {
                $arrTypeExtraInt[$val] = trim($arr[$key]);
            } else
                $arrTypeExtraInt[$val] = 'no';
        }
        $str = array_merge($arrTypeId, $arrTypeExtraInt, $arrTypeExtraBool);
    }
    return $str;
}

// *********  метод для отправки запроса в БД (возврашает результат запроса)
function Request($strRequest)
{
    $link = new mysqli(HOST, USERNAME, PASSWORD, DB_NAME);
    $link->set_charset('utf8');     // устанавливаем кодировку
    if (!$link)
        die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

    $isOk = $link->query($strRequest);
    if ($isOk) {
        return $isOk;
    } else {
        echo "Query error.Error({$link->connect_errno}):{$link->error}<br/>";
        return $isOk;
    }
    $link->close();
}

function GetDataProduct($arrUrl)
{
    global $arrInTable;
    $arrData = array();                      // массив с товарими
    foreach ($arrUrl as $val) {
        $dom = file_get_html($val);          // получаем массив элементов на странице (DOM)

        $arrProduct = array();               // массив данных о товаре
        //_______ получаем название
        $arrProduct['name'] = GetName($dom);
        //_______ получаем id региона
        $arrProduct['id_region'] = GetIdRegion($dom);
        //_______ получаем статус
        $arrProduct['id_state'] = GetStatus($dom, $arrInTable);
        //_______ получаем стоимость
        $arrProduct['price'] = GetPrice($dom);
        //_______ получаем характеристики
        $arrProduct['type'] = GetTypeId($dom);
        $arrData[] = $arrProduct;

        $dom->clear();
        unset($dom);
    }
    return $arrData;
}

// 0.В базе данных создаем таблицу с полями id, name, isParse(varchar(3))
// при заполнении таблицы ссылками проставляем значение isParse = no

// 1.Открываем БД и читаем N ссылок со значением в столбце isParse = no для парсинга за 1 раз
$strRequest = "SELECT id,name FROM " . TABLENAME . " WHERE isPars = 'no' lIMIT " . LIMIT;
$result = Request($strRequest);
$arrResult = array();
if ($result) {
    for ($i = 0; $i < $result->num_rows; $i++) {
        $row = $result->fetch_assoc();
        $arrResult[$row['id']] = $row['name'];
    }
}
// выводим массив с сылками (для отладки)
echo "<pre>";
print_r($arrResult);
echo "<pre/>";

// 2.Отправляем массив с сылками в функцию
if (count($arrResult) != 0)
    $res = GetDataProduct($arrResult);
else {
    echo "All links from the database worked !!!";
    die();
}

$strRequest = "INSERT INTO product (name,id_region,id_state,price,id_type,id_bedrooms,id_furniture,id_swimming,seaDistance," .
    "coveredSquare,allSquare,shopDistance,is_isAir_conditioning,is_isVeranda,is_isChild_chair,is_isCot,is_Safety_system," .
    "is_isBalcony,is_Heating_floor,is_isPlayground,is_isCentral_heating, is_isSolar_battery,is_isDishwasher,is_isParking, " .
    "is_Equipped_kitchen,is_isPet, is_isPantry,is_isWasher,is_isTV,is_isInternet,is_isFireplace,is_IsBBQ,is_isGarden,is_isArbor," .
    "is_isSauna,is_isJacuzzi,is_isTennis_court" .
    ") VALUES ";
$strRes = null;
foreach ($res as $key => $valProd) {
    $strAddRegion = "('";
    foreach ($valProd as $key => $val) {
        if ($key == 'type') {
            foreach ($val as $valType) {
                $strAddRegion .= $valType . "','";
            }
            $strAddRegion = substr_replace($strAddRegion, "", -2);
            $strAddRegion .= ")," . PHP_EOL;
            break;
        }
        $strAddRegion .= $val . "','";
    }
    $strRes .= $strAddRegion;
}

$strRequest .= $strRes;
$strRequest = substr_replace($strRequest, "", -2);

//echo "" . "<br/><br/>";
//echo "$strRequest" . "<br/><br/>";
//echo "<pre>";
//print_r($res);
//echo "</pre>";

if (Request($strRequest))
    echo "Products add !!! " . "<br/>";

// 3. Помечаем ссылки в таблице как отработанные
$strUpdate = "UPDATE " . TABLENAME . " SET isPars = 'yes' WHERE id in (";
foreach ($arrResult as $key => $val)
    $strUpdate .= "'" . $key . "',";
$strUpdate = substr($strUpdate, 0, -1);
$strUpdate .= ")";

// 4.Отправляем запрос и получаем результат
if (Request($strUpdate))
    echo "Done !!!";


