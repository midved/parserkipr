<?php
// ********* метод сканирует страницы и записывает текущюю ссылку в файл
// *** при следующем запуске начинает сканировать с последней страницы на которой остановился

define('FILENAME', 'lastHref.txt'); // в файл сохраняем ссылку на пропарсенную страницу
// ссылка с которой начинаем поиск
define('URL', 'http://cyprus-realty.testsite.co.ua/prodazha-nedvizhimosti-na-kipre');

// ********* константы для подключения к бд
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', 'flexi140');
define('DB_NAME', 'dbkipr2');

// ********* библиотека для парсинга
include 'simple_html_dom.php';

class Region
{
    private $id_country;
    private $name;

    public function __construct($countryName, $regionName)
    {
        $this->id_country = $this->GetIdCountryByName($countryName);
        $this->name = $regionName;
    }

    // метод по отправке данных в таблицы
    private function Request($strRequest)
    {
        $link = new mysqli(HOST, USERNAME, PASSWORD, DB_NAME);
        $link->set_charset('utf8');     // устанавливаем кодировку
        if (!$link)
            die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

        $isOk = $link->query($strRequest);
        if ($isOk) {
            return $isOk;
        } else {
            echo "Query error.Error({$link->connect_errno}):{$link->error}<br/>";
            return $isOk;
        }
        $link->close();
    }

    private function GetIdCountryByName($countryName)
    {
        $id = null;
        $result = $this->Request("SELECT country.id FROM `country` WHERE country.name='" . $countryName . "'");
        if ($result) {
            $id = ($result->fetch_row())[0];
            if (!$id) $id = 0;
        }
        return $id;
    }

    public function GetCountryId()
    {
        return array($this->id_country, $this->name);
    }
}


class Product
{
    private $href;                          // ссылка на товар
    private $id_region;                     // регион+страна
    private $name;
    private $status;                        // статус (готово к заселению)
    private $price;
    private $arrExtraOptions;               // маассив с доп свойствами

    public function __construct($url)
    {
        $this->href = $url;
    }
    public function GetProductId()
    {
        $id = null;
        $isInTable = $this->Request("SELECT id FROM product WHERE href = '$this->href' ;");
        if ($isInTable) {
            $row = $isInTable->fetch_assoc();
            $id = $row['id'];
        }
        return $id;
    }

    public function Request($strRequest) // метод по отправке данных в таблицы
    {
        $link = new mysqli(HOST, USERNAME, PASSWORD, DB_NAME);
        $link->set_charset('utf8');     // устанавливаем кодировку
        if (!$link)
            die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

        $isOk = $link->query($strRequest);
        if ($isOk) {
            return $isOk;
        } else {
            echo "Query error.Error({$link->connect_errno}):{$link->error}<br/>";
            return $isOk;
        }
        $link->close();
    }

    private function GetDataTable($tableName, $arrData)  // метод получает данные из таблицы $tableName
    {
        // получаем данные которые уже есть в таблице
        $arrInTable = array();
        $isInTable = $this->Request("SELECT name FROM $tableName ;");
        if ($isInTable) {
            for ($i = 0; $i < $isInTable->num_rows; $i++) {
                $row = $isInTable->fetch_assoc();
                //echo "{$row['name']}" . "<pre>";
                $arrInTable[$row['name']] = 1;
            }
        }

        // исключаем из $arrData регионы которые уже есть в таблице ($arrInTable)
        $arrResult = array();
        foreach ($arrData as $key => $val) {
            if (!array_key_exists($key, $arrInTable))
                $arrResult[trim($key)] = trim($val);
        }
        // возврашаем массив новых уникальных имен

        return $arrResult;
    }

    public function GetId($nameRequest, $tableName) // метод получает имя для поиска и имя таблицы, возврашает id
    {
        $id = null;
        $isInTable = $this->Request("SELECT id FROM " . $tableName . " WHERE name ='" . trim($nameRequest) . "';");
        if ($isInTable) {
            $row = $isInTable->fetch_assoc();
            $id = $row['id'];
        }
        return $id;
    }

    public function AddHrefInTable() // метод добавляет уникальные ссылки на товар в таблицу
    {
        $arr = array($this->href); // передаем ссылку как массив, тк метод принимает массив
        $Result = $this->GetDataTable('product', $arr);
        if ($this->Request("INSERT INTO product (href) VALUES('$Result[0]')"))
            echo "Добавлена ссылка с товаром !!! " . "<br/>";
    }

    public function AddRegionsTable() // метод добавляет регион в таблицу если его нет и если он уже там есть возврашает его id
    {
        $arrData = array();                                              // в массив будем добавлять результаты поиска по ссылкам
        $regionName = null;                                              // имя региона
        // _____  получаем значение региона на странице
        $dom = file_get_html($this->href);                               // получаем массив элементов на странице (DOM)
        $arrSearchResults = $dom->find('li.loc span.text-info');         // получаем массив c результатом поиска
        if (count($arrSearchResults) == 0) {                             // если поиск ничего не дал переходим дальше
            echo "No results for this query";
        } else {
            //___ здесь выполняем основную работу над результатом и заполняем массив $arrData
            foreach ($arrSearchResults as $element) {
                $arrCountryRegion = explode(",", $element->plaintext);
                if (count($arrCountryRegion) == 1) {                                       // если у страны нет региона
                    $newRegion = new Region($arrCountryRegion[0], $arrCountryRegion[0]);
                    $arrData[trim($newRegion->GetCountryId()[1])] = trim($newRegion->GetCountryId()[0]);
                    $regionName = $arrCountryRegion[0];
                } else {
                    $newRegion = new Region($arrCountryRegion[0], $arrCountryRegion[1]);
                    $arrData[trim($newRegion->GetCountryId()[1])] = trim($newRegion->GetCountryId()[0]);
                    echo $newRegion->GetCountryId()[0] . "<<>>" . $newRegion->GetCountryId()[1] . "<br/><br/>";
                    $regionName = $arrCountryRegion[1];
                }
            }
        }
        // _____ значения уникальных имен которых нет в таблице регионов
        $arrResult = $this->GetDataTable('region', $arrData);

        $strAddRegion = "INSERT INTO region (id_country,name) VALUES";
        foreach ($arrResult as $key => $val)
            $strAddRegion .= "('" . trim($val) . "','" . trim($key) . "'),";
        $strAddRegion = substr_replace($strAddRegion, ";", -1);

        if ($this->Request($strAddRegion))
            echo "Добавлены регионы !!! " . "<br/>";

        $dom->clear();
        unset($dom);

        $this->id_region = $this->GetId($regionName, 'region');
    }

    private function GetName($dom) //__________________ получаем название
    {
        $str = null;
        $arrSearchResults = $dom->find('h1');
        if (count($arrSearchResults) == 0) {
            echo "The selector GetName status failed" . "<br>";
            return null;
        } else
            $str = $arrSearchResults[0]->plaintext;
        return $str;
    }

    private function GetStatus($dom) //__________________ получаем статус
    {
        $str = null;
        $status = $dom->find('li.offer2', 0);
        $str = explode(":",trim($status->plaintext));
        return $str[1];
    }

    private function GetPrice($dom)
    {
        $str = null;
        $arrSearchResults = $dom->find('li. span.main-price');
        if (count($arrSearchResults) == 0) {
            echo "The selector GetPrice status failed" . "<br>";
            return null;
        } else {
            $str = trim(mb_substr($arrSearchResults[0]->plaintext, 2));    // удаляем символы с начала строки
        }
        return $str;
    }

    private function GetType($dom)
    {
        $arrSearchResults = $dom->find('div.table-responsive table tbody'); // находим всю таблицу со всеми характеристиками
        if (count($arrSearchResults) == 0) {
            echo "The selector GetTypeId status failed" . "<br>";
            return null;
        }
            $arrTypes = array();
            foreach ($arrSearchResults[0]->find('td') as $val)             // находим строки таблицы
                $arrTypes[] = $val->plaintext;

            $arrTypes = array_filter($arrTypes, function ($el) {           // убираем пустые элементы
                return !empty($el);
            });

            $arr = array();                                               // основные параметры
            $arrTypes = array_chunk($arrTypes, 2);                   // разбиваем на несколько массивов по 2
            foreach ($arrTypes as $key => $val) {
                $arrStr = explode(":", implode($val));
                $arr[$arrStr[0]] = $arrStr[1];
            }

        //_______ удаляем доп функции и сохраняем в массив
        $arrExtra = array_pop($arrTypes);
        $arrExtra = explode("&bull", $arrExtra[1]);

        foreach ($arrExtra as $val) {
            $str = trim(str_replace(";", "", $val));
            $this->arrExtraOptions[$str] = $str;
        }

        $arrExtraRes = $this->GetDataTable("extraOption",$this->arrExtraOptions); // проверяем уникальные значения
        if(count($arrExtraRes)>0){
            //_______  записываем уникальные данные в таблицу
            $str = "INSERT INTO extraOption (name) VALUES";
            foreach ($arrExtraRes as $val)
                $str.="('".$val."'),";
            $str =  substr($str, 0, -1);                       // удаляем последний символ
            $this->Request($str);
        }

            $arrNameOptions = array(
                'Тип недвижимости'=>'null',
                'Спальни'=>'null',
                'Расстояние до моря'=>'null',
                'Ванные'=>'null',
                'Крытая площадь'=>'null',
                'Бассейн'=>'null',
                'Год постройки'=>'null',
                'Мебель'=>'null',
                'Площадь участка'=>'null',
                'Расстояние до супермаркета'=>'null'
            );
            $arrType = array();                                            // получаем ключ значение опций
            foreach ($arrTypes as $key=>$val) {
                foreach ($val as $key => $item) {
                    $arrStr = explode(":", implode($val));
                    $arrType[$arrStr[0]]= $arrStr[1];
                }
            }
            foreach ($arrType as $key=> $val)                            // подставляем значения в наш массив
                $arrNameOptions[$key]= $val;

             return $arrNameOptions;

    }
    private function AddProductInTable($arrType)
    {
      $arrTableName = array("type","bedrooms","distanceSea","bath","square","swiming","year","furniture","allSquare","distanceShop" );
      $arrType=array_combine($arrTableName,$arrType);
      $strRequest="UPDATE product SET name='$this->name', id_region='$this->id_region', status='$this->status',price='$this->price',";

      foreach ($arrType as $key=>$val)
          $val!='null'?$strRequest.=" $key = '$val' ,":$strRequest.=" $key = $val,";

      $strRequest.= " isParse = 'ок' ";
       // $strRequest = substr($strRequest, 0, -1); // удаляем последний символ
        $strRequest.= " WHERE href = '$this->href'";

      if($this->Request($strRequest))
          echo "Product ADD !!! " . "<br/><br/>";
    }

    public function GetAllProduct() // метод получает остальные данные о продукте
    {
        $dom = file_get_html($this->href);          // получаем массив элементов на странице (DOM)

        //_______ получаем название
        $this->name = $this->GetName($dom);

        //_______ получаем статус
        $this->status = $this->GetStatus($dom);

        //_______ получаем стоимость
        $this->price = $this->GetPrice($dom);

        //_______ получаем характеристики
        $arrProduct = $this->GetType($dom);

        $this->AddProductInTable($arrProduct);      // загружаем данные в таблицу

        $dom->clear();
        unset($dom);
    }

    public function GetExtraOption() // метод возврашает id доп опций
    {
        $arrExtraId = array();
        foreach ($this->arrExtraOptions as $val)
            $arrExtraId[]= $this->GetId($val,"extraOption");
        return $arrExtraId;
    }

}// конец класса product


function GetHrefInPage($url = URL, $page = 1)
{
    $arrResult = array();                                     // промежуточный массив - сохраняем результаты с for
    for ($i = 0; $i < 1; $i++) {                              // за один цикл while парсим 1 страницу
        $u = file_get_html($url);                             // получаем массив элементов на странице (DOM)
        $arrUrlRes = $u->find('h4 a');                        // получаем массив нужных ссылок на странице
        if (count($arrUrlRes) == 0) {                         // если ссылок нет выходим со всех циклов - конец парсинга
            $flag = false;
            break;
        }
        foreach ($arrUrlRes as $urls)                         // заполняем массив ссылками
            $arrResult[] = $urls->href;
        $page++;                                              // перелистываем страницу
        $url = URL . "/?page=" . $page;                       // меняем урл
        file_put_contents(FILENAME, $url);           // записываем новый урл в файл
    }
    foreach ($arrResult as $val) {
        $product = new Product($val);
        $product->AddHrefInTable();               // получаем и записываем ссылку в бд
        $product->AddRegionsTable();              // получаем или записываем и получаем id_regiona в поля класса
        $product->GetAllProduct();                // получаем все остальные свойства

        //________ получаем id товара по его ссылке
        $idProduct = $product->GetProductId();
        // _______ получаем доп данные товара (id)
        $extraId = $product->GetExtraOption();
        if(count($extraId)>0)
        {
            $str = "INSERT INTO productValue (id_product, id_extra) VALUES ";
            foreach ($extraId as $val)
                $str.="($idProduct, $val),";
            $str = substr($str, 0, -1);          // удаляем последний символ

          if($product->Request($str))
              echo "ProductValue ADD OK!" . "<br/><br/>";
          else  echo "ProductValueNotADD OK!" . "<br/><br/>";
        }
        else{
            if($product->Request("INSERT INTO productValue (id_product, id_extra) VALUES ($idProduct ,null)"))
                echo "ProductValue ADD OK!" . "<br/><br/>";
            else  echo "ProductValueNotADD OK!" . "<br/><br/>";
        }
    }
}

// вызываем метод и заполняем в бд таблицу href
$file = file_get_contents(FILENAME);     // открываем и читаем файл
if ($file != "") {
    $pageSearch = explode("=", $file);  // получаем страницу
    GetHrefInPage($file, $pageSearch[1]);          // передаем в метод ссылку и номер страницы
} else
    GetHrefInPage();










//
//
//private $type = "no_type";                // тип - вилла
//private $bedrooms = "no_bedrooms";        // кол-во спален
//private $distanceSea = 0;                 // расстояние до моря
//private $bath = 0;                        // ванные
//private $square = 0;                      // крытая площадь
//private $swiming = "no_swiming";          // тип басейна (общий/частный/нет)
//private $year = 0;                        // год постройки
//private $furniture = "no_furniture";      // мебель
//private $allSquare = 0;                   // площадь участка
//private $distanceShop = 0;                // расстояние до магазина
