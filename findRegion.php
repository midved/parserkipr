<?php
include 'simple_html_dom.php';    // библиотека для парсинга

// ********* константы режимов парсинга
define('TABLENAME', 'href');       // имя таблицы где храним ссылки
define('LIMIT', '100');              // количество ссылок получаемое из бд за одну загрузку скрипта

// ********* константы для подключения к бд
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', 'flexi140');
define('DB_NAME', 'dbkipr');

class Region
{
    private $id_country;
    private $name;

    public function __construct($countryName, $regionName)
    {
        $this->id_country = $this->GetIdCountryByName($countryName);
        $this->name = $regionName;
    }

    private function GetIdCountryByName($countryName)
    {
        $id = null;
        $result = Request("SELECT country.id FROM `country` WHERE country.name='" . $countryName . "'");
        if ($result) {
            $id = ($result->fetch_row())[0];
            if (!$id) $id = 0;
        }
        return $id;
    }

    public function GetCountryId()
    {
        return array($this->id_country, $this->name);
    }
}

// *********  метод для отправки запроса в БД (возврашает результат запроса)
function Request($strRequest)
{
    $link = new mysqli(HOST, USERNAME, PASSWORD, DB_NAME);
    $link->set_charset('utf8');     // устанавливаем кодировку
    if (!$link)
        die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

    $isOk = $link->query($strRequest);
    if ($isOk) {
        return $isOk;
    } else {
        echo "Query error.Error({$link->connect_errno}):{$link->error}<br/>";
        return $isOk;
    }
    $link->close();
}


function GetHrefInPage($arrUrl)
{
    $arrData = array();                  // в массив будем добавлять результаты поиска по ссылкам
    $dom = null;
    foreach ($arrUrl as $val) {
        $dom = file_get_html($val);                 // получаем массив элементов на странице (DOM)
        $arrSearchResults = $dom->find('li.loc span.text-info');        // получаем массив c результатом поиска
        if (count($arrSearchResults) == 0) {          // если поиск ничего не дал переходим дальше
            echo "No results for this query";
            continue;
        } else {
            //___ здесь выполняем основную работу над результатом и заполняем массив $arrData
            foreach ($arrSearchResults as $element) {
                $arrCountryRegion = explode(",", $element->plaintext);
                 if (count($arrCountryRegion) == 1){                                       // если у страны нет региона
                     $newRegion = new Region($arrCountryRegion[0], $arrCountryRegion[0]);
                     $arrData[trim($newRegion->GetCountryId()[1])] = trim($newRegion->GetCountryId()[0]);
                }
                else {
                    $newRegion = new Region($arrCountryRegion[0], $arrCountryRegion[1]);
                    $arrData[trim($newRegion->GetCountryId()[1])] = trim($newRegion->GetCountryId()[0]);
                   // echo $newRegion->GetCountryId()[0] . "<<>>" . $newRegion->GetCountryId()[1] . "<br/><br/>";
                }
            }
            //___
        }
    }
     // получаем регионы которые уже есть в таблице
    $arrInTable = array();
    $isInTable = Request("SELECT name FROM region;");
    if ($isInTable) {
        for ($i = 0; $i < $isInTable->num_rows; $i++) {
            $row = $isInTable->fetch_assoc();
            //echo "{$row['name']}" . "<pre>";
            $arrInTable[$row['name']] = 1;
        }
    }

    // исключаем из $arrData регионы которые уже есть в таблице ( $arrInTable)
    $arrResultat = array();
    foreach ($arrData as $key=>$val) {
        if(!array_key_exists($key,$arrInTable))
            $arrResultat[trim($key)]=trim($val);
    }


    $strAddRegion = "INSERT INTO region (id_country,name) VALUES";
    foreach ($arrResultat as $key => $val)
        $strAddRegion .= "('" .trim($val) . "','" . trim($key) . "'),";
    $strAddRegion = substr_replace($strAddRegion, ";", -1);

    //echo "$strAddRegion" . "<br/><br/>";

    if (Request($strAddRegion))
        echo "Добавлено !!! " . "<br/>";

    $dom->clear();
    unset($dom);


// 3. Помечаем ссылки в таблице как отработанные
    global $arrResult;
    $strUpdate = "UPDATE " . TABLENAME . " SET isPars = 'yes' WHERE id in (";
    foreach ($arrResult as $key => $val)
        $strUpdate .= "'" . $key . "',";
    $strUpdate = substr($strUpdate, 0, -1);
    $strUpdate .= ")";

// 4.Отправляем запрос и получаем результат

    if (Request($strUpdate))
        echo "Done !!!";
    return $arrData;
}

// 0.В базе данных создаем таблицу с полями id, name, isParse(varchar(3)) 
// при заполнении таблицы ссылками проставляем значение isParse = no

// 1.Открываем БД и читаем N ссылок со значением в столбце isParse = no для парсинга за 1 раз
$strRequest = "SELECT id,name FROM " . TABLENAME . " WHERE isPars = 'no' LIMIT " . LIMIT;
$result = Request($strRequest);
$arrResult = array();
if ($result) {
    for ($i = 0; $i < $result->num_rows; $i++) {
        $row = $result->fetch_assoc();
        $arrResult[$row['id']] = $row['name'];
    }
}

// выводим массив с сылками (для отладки)
// echo "<pre>";
// print_r($arrResult);
// echo "<pre/>";

// 2.Отправляем массив с сылками в функцию
if (count($arrResult) != 0)
    GetHrefInPage($arrResult);
else {
    echo "All links from the database worked !!!";
    die();
}
