<?php
include 'simple_html_dom.php';               // библиотека для парсинга
// константы для подключения к бд
define('HOST', 'localhost');
define('USERNAME', 'roo8t');
define('PASSWORD', 'fle7xi140');
define('DB_NAME', 'dbkipr9');

// ссылка с которой начинаем поиск
define('URL', 'http://cyprus-realty.testsite.co.ua/prodazha-nedvizhimosti-na-kipre');

// *********  метод отправляет запрос
function Request($strRequest)
{
    $link = new mysqli(HOST, USERNAME, PASSWORD, DB_NAME);
    $link->set_charset('utf8');     // устанавливаем кодировку
    if (!$link)
        die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

    $isOk = $link->query($strRequest);
    if ($isOk) {
        return $isOk;
    } else {
        echo "Query error.Error({$link->connect_errno}):{$link->error}<br/>";
        return $isOk;
    }
    $link->close();
}

// *********  метод находит данные для таблиц в бд
function Search($isSearch, $isCreateTable)
{
    // массивы с данными таблиц
    $arrTablesKeysName = array("country", "region", "type", "bedrooms", "swimming", "furniture", "state");  // масив с названием таблиц в бд
    $arrTablesValue = array(array(), array(), array(), array(), array(), array(), array());                 // масив со значениями таблиц в бд
    $arrExtraOptions = array();                                                                             // массив с "дополнительные параметры"

    $html = file_get_html(URL);
    $i = 0;
    $j = 1;
    // ищем данные для таблиц
    if ($isSearch) {
        foreach ($html->find('select option') as $element) {
            if ($element->plaintext != "Все") {
                $arrTablesValue[$i][$j] = $element->plaintext;
                $j++;
            } else {
                $i++;
                $j = 1;
            }
        }
        array_shift($arrTablesValue);
        // ___ меняем ключи массив - (ключ->имя_таблицы, значения-> данные таблицы + ключ->id)
        $arrTablesValue = array_combine($arrTablesKeysName, $arrTablesValue);
        // ___ ищем "дополнительные параметры"
        foreach ($html->find('div.option-values label') as $elements)
            array_push($arrExtraOptions, $elements->plaintext);
        // ___ подчищаем
        $html->clear();
        unset($html);
    }

        ////массивы с данными (для отладки)
        echo "<pre>";
        print_r($arrTablesValue);
        echo "</pre>";

        echo "<pre>";
        print_r($arrExtraOptions);
        echo "</pre>";

    if ($isCreateTable) {
        // ___ запросы на создание таблиц (кроме таблицы регионов)
        foreach ($arrTablesValue as $tableName => $tableVal) {
            if ($tableName == 'swimming')
            {

            }
            if ($tableName == 'region')
                continue;
            Request("CREATE TABLE $tableName (id integer not null primary key auto_increment, name varchar(50))DEFAULT CHARSET = utf8;");
            foreach ($tableVal as $val)
                Request("INSERT INTO $tableName (name) VALUES ('" . $val . "')");
        }
        // ___ запрос на создание таблицы region
        Request("CREATE TABLE region (" .
            "id integer not null primary key auto_increment," .
            "id_country int not null," .
            "name varchar(50)," .
            "CONSTRAINT Country FOREIGN KEY (id_country) REFERENCES country (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION" .
            ")DEFAULT CHARSET = utf8;");

        // ___ запрос на создание таблицы c сылками
        Request("CREATE TABLE href (" .
            "id integer not null primary key auto_increment," .
            "name varchar(100)," .
            "isPars varchar(3)," .
            ")DEFAULT CHARSET = utf8;");

        // ___ запрос на создание таблицы c товарами
        Request("CREATE TABLE product(" .
            "id int NOT NULL PRIMARY KEY AUTO_INCREMENT," .
            "name 						varchar(300)," .
            "id_region 					int NOT NULL," .
            "id_state 					int NOT NULL," .
            "price 						varchar(10)," .
            "id_type 					int NOT NULL," .
            "id_bedrooms 				int NOT NULL," .
            "id_furniture 				int NOT NULL," .
            "id_swimming 				int NOT NULL," .
            "seaDistance 				varchar(10)," .
            "coveredSquare 				varchar(20)," .
            "allSquare  				varchar(20)," .
            "shopDistance 				varchar(10)," .
            "is_isAir_conditioning      varchar(3)," .
            "is_isVeranda               varchar(3)," .
            "is_isChild_chair           varchar(3)," .
            "is_isCot   				varchar(3)," .
            "is_Safety_system    		varchar(3)," .
            "is_isBalcony    			varchar(3)," .
            "is_Heating_floor    		varchar(3)," .
            "is_isPlayground   			varchar(3)," .
            "is_isCentral_heating   	varchar(3)," .
            "is_isSolar_battery   		varchar(3)," .
            "is_isDishwasher   			varchar(3)," .
            "is_isParking   			varchar(3)," .
            "is_Equipped_kitchen   		varchar(3)," .
            "is_isPet  					varchar(3)," .
            "is_isPantry   				varchar(3)," .
            "is_isWasher    			varchar(3)," .
            "is_isTV    				varchar(3)," .
            "is_isInternet   			varchar(3)," .
            "is_isFireplace   			varchar(3)," .
            "is_IsBBQ   				varchar(3)," .
            "is_isGarden 				varchar(3)," .
            "is_isArbor    				varchar(3)," .
            "is_isSauna    				varchar(3)," .
            "is_isJacuzzi   			varchar(3)," .
            "is_isTennis_court   		varchar(3)," .

            "CONSTRAINT Region FOREIGN KEY (id_region) REFERENCES region (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION," .

            "CONSTRAINT State FOREIGN KEY (id_state) REFERENCES state (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION," .

            "CONSTRAINT Type FOREIGN KEY (id_type) REFERENCES  type (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION," .

            "CONSTRAINT Bedrooms FOREIGN KEY (id_bedrooms) REFERENCES  bedrooms (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION," .

            "CONSTRAINT Furniture FOREIGN KEY (id_furniture) REFERENCES  furniture (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION," .

            "CONSTRAINT Swimming FOREIGN KEY (id_swimming) REFERENCES  swimming (id)" .
            "ON UPDATE NO ACTION" .
            "ON DELETE NO ACTION" .
            ")DEFAULT CHARSET = utf8;");// конец Request()
    }
}