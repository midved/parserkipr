<?php
// ********* метод сканирует страницы и записывает текущюю ссылку в файл
// *** при следующем запуске начинает сканировать с последней страницы на которой остановился

define('FILENAME', 'lastHref.txt'); // в файл сохраняем ссылку на пропарсенную страницу
// ссылка с которой начинаем поиск
define('URL', 'http://cyprus-realty.testsite.co.ua/prodazha-nedvizhimosti-na-kipre');

function GetHrefInPage($url = URL, $page = 1)
{
//    $flag = true;                                               // флаг окончания ссылок с товарами
//    while ($flag) {
        $arrResult= array();                                      // промежуточный массив - сохраняем результаты с for
        for($i=0;$i<1;$i++) {                                     // за один цикл while парсим 5 страниц
            $u = file_get_html($url);                             // получаем массив элементов на странице (DOM)
            $arrUrlRes = $u->find('h4 a');                // получаем массив нужных ссылок на странице
            if (count($arrUrlRes) == 0) {                         // если ссылок нет выходим со всех циклов - конец парсинга
                $flag = false;
                break;
            }
            foreach ($arrUrlRes as $urls)                         // заполняем массив ссылками
                $arrResult[] = $urls->href;
            $page++;                                              // перелистываем страницу
            $url = URL . "/?page=" . $page;                       // меняем урл
            file_put_contents(FILENAME, $url);           // записываем новый урл в файл
        }
        //___ создаем текст запроса и формируем строку для вставки в бд
        $str = "INSERT INTO href (name) VALUES";
        foreach ($arrResult as $val){
            //$str.="('".$val."'),";
            echo "$val" . "<br/><br/>";
        }

        $str = substr_replace($str,";",-1);


//        if( Request($str))
//            echo "Добавлено !!! "."<br/>";
//        else echo "<br/>Ошибка записи ссылок в бд !!!. Проверьте файл lastHref.txt. Возможно в нем прописана последняя ссылка<br/> ";

// }
}

// вызываем метод и заполняем в бд таблицу href
$file = file_get_contents(FILENAME);     // открываем и читаем файл
if ($file != "") {
    $pageSearch = explode("=", $file);  // получаем страницу
    GetHrefInPage($file, $pageSearch[1]);          // передаем в метод ссылку и номер страницы
} else
    GetHrefInPage();